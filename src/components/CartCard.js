import {useState, useEffect,useContext} from 'react'



import {Card,Row,Col,Button} from 'react-bootstrap'
import {Link, NavLink,Navigate} from 'react-router-dom'
import { Form } from 'react-bootstrap';
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function CartCard({product}) {

	const { user } = useContext(UserContext)
	const {productName, quantity, subTotal, _id,productId} = product


	const [isClicked,setIsClicked] = useState(false)
	const [quantities,setQuantity] = useState(quantity)
	const [subTotals,setSubTotals] = useState(subTotal)
	const [isDelete,setIsDelete] = useState(false)
	let price = subTotal / quantity

	const token = localStorage.getItem('token')




	const clickCard = ()=>{
		setIsClicked(true);
	}
	const noNegative = (e) => {



		if(e.target.value > 0){
			setIsDelete(false)
		}else{
			setIsDelete(true)
		}

		if(e.target.value<0){
			e.target.value=0
			setQuantity(e.target.value)

		}
		else{		

			setQuantity(e.target.value)
			setSubTotals(price * e.target.value)
			console.log(quantities)
		}
	}



	function updateQuantity (e) {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/updatequantity`,{

			'method': 'PATCH',

			headers:{

				'Authorization': `Bearer ${token}`,
				'Content-Type': 'application/json'
			
			},
			body: JSON.stringify({

				itemId: _id,
				quantity: quantities
				
			})

		}).then(res => res.json()).then(data=> {
			console.log(data)

			Swal.fire({
				title: "Update Success",
				icon: "success",
				text: "Success!"
			})

		})

	}

	function removeItem (e) {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/deleteitem`,{
			'method': 'PATCH',
			headers:{

				'Authorization': `Bearer ${token}`,
				'Content-Type': 'application/json'
			
			},
			body: JSON.stringify({

				itemId: _id
				
			})
		}).then(res => res.json()).then(data=> {
			console.log(data)

			Swal.fire({
				title: "Deleted",
				icon: "success",
				text: "Item Deleted"
			})

		})

	}

	return (
		(isClicked) ? 	
		<Navigate to={`/products/${productId}`}/>
		:
		<>
		<Card className="m-2">
		<Card.Body>
		<Card.Title onClick={clickCard} style={{cursor: 'pointer'}} className="cardHighlight p-3" >
		<h2>{productName}</h2>
		</Card.Title>
		<Card.Subtitle className="mb-2">
		<strong>Quantity: </strong>
		</Card.Subtitle>
		<Form.Control type="number" placeholder="Quantity" className="text-center mb-2" value= {quantities} onChange= {noNegative} style={{width:100}}
		required
		/>

		<Card.Subtitle className="mb-2">
		Subtotal:
		</Card.Subtitle>
		<Card.Text>
		PHP {subTotals}
		</Card.Text>

		{
			(isDelete) ?
			<Button className="m-3"  variant="danger" onClick={removeItem}>Remove</Button>
			:
			<>
			<Button className="m-3"  variant="warning" onClick={updateQuantity} >Update</Button>
			<Button className="m-3"  variant="danger" onClick={removeItem} >Remove</Button>
			</>
		}

		</Card.Body>
		</Card>
		</>

		
		)
}