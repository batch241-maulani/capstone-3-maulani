import { useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';

import {useParams, Link} from 'react-router-dom'

import UserContext from '../UserContext'

import SingleCheckOutView from './SingleCheckOutView'

import Form from 'react-bootstrap/Form'

import {Navigate} from 'react-router-dom'

import Swal from 'sweetalert2'

let subTotal


export default function ProductView() {

	const {user} = useContext(UserContext)

	const {_id} = useParams()
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [linkImage, setLinkImage] = useState("");
	const [stock,setStock] = useState(0)
	const [isActive,setIsActive] = useState(true)
	const [checkOuts,setCheckOuts] = useState(false)
	const [isCheckOutEnabled,setIsCheckOutEnabled] = useState(false)

	const token = localStorage.getItem('token')
	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`).then(res => res.json()).then(data=>{
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setStock(data.stock)
			setLinkImage(data.imagelink)
			setIsActive(data.isActive)


		})
		
		console.log(token)


	}, [_id])



	function updateProduct (e) {

		e.preventDefault()

		console.log('Clicked')

    	fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`,{
    		'method': 'PUT',
    		headers:{
    			Authorization: `Bearer ${token}`,
    			'Content-Type': 'application/json'
    		},
    		body: JSON.stringify({
    			name: name,
    			description: description,
    			stock: stock,
    			price: price,
    			imagelink: linkImage,
    			isActive: isActive
    		})
    	}).then(res => res.json()).then(data=> {
    		console.log(data)

    		Swal.fire({
    			title: "Product Updated Successfully!",
    			icon: "success",
    			text: "Updated."
    		})

    	})



	}
	

	const noNegative = (e) => {
		if(e.target.value>0)
		{
			setIsCheckOutEnabled(true)
		} else{
			setIsCheckOutEnabled(false)
		}
		if(e.target.value<0){
			e.target.value=0

		
		}
		else{



		}
	}
	const isCheckOut = () => {
		if(!checkOuts){
			setCheckOuts(true)
		}else{
			setCheckOuts(false)
		}

	}
	const setIsActiveValue=(e) => {
		if(e.target.value){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
		console.log(isActive)

	}
	
	return (

		<>
		<Container>
		<Row>
		<Col lg={{span: 6, offset:3}} >
		<Card>
		<Card.Img variant="top" src={linkImage}/>
		<Card.Body className="text-center">
		<Form.Group controlId="productName">
		<Form.Label>Name</Form.Label>
		<Form.Control 
		type="text" 
		placeholder={name} 
		value= {name}
		onChange = {e => setName(e.target.value)}
		required
		/>
		</Form.Group>
		<Form.Group controlId="productDescription">
		<Form.Label>Description</Form.Label>
		<Form.Control 
		type="text" 
		placeholder={description} 
		value= {description}
		onChange = {e => setDescription(e.target.value)}
		required
		/>
		</Form.Group>
		<Form.Group controlId="productPrice">
		<Form.Label>Price</Form.Label>
		<Form.Control 
		type="number" 
		placeholder={price} 
		value= {price}
		onChange = {e => setPrice(e.target.value)}
		required
		/>
		</Form.Group>
		<Form.Group controlId="productStock">
		<Form.Label>Stock</Form.Label>
		<Form.Control 
		type="number" 
		placeholder={stock} 
		value= {stock}
		onChange = {e => setStock(e.target.value)}
		required
		/>
		</Form.Group>
		<Form.Group controlId="productImageLink">
		<Form.Label>Image Link</Form.Label>
		<Form.Control 
		type="text" 
		placeholder={linkImage} 
		value= {linkImage}
		onChange = {e => setLinkImage(e.target.value)}
		required
		/>
		</Form.Group>

		<Form>
		<Form.Label>Is Active</Form.Label>
		<Form.Group controlId="productImageLink">
			{
				(isActive) ? 
				<>
			 <Form.Check name="radioActive" type={'radio'} id={'isActive-true'} label={'True'} value={true}  onChange={e=> {
			 	setIsActive(true)
			 	console.log(isActive)
			 }} defaultChecked />
			 <Form.Check name="radioActive" type={'radio'} id={'isActive-false'} value={false} label={'False'}  onChange={e=> {
			 	setIsActive(false)
			 	console.log(isActive)
			 }}



			 />
			 </>
			 :
			 <>
			  <Form.Check name="radioActive" type={'radio'} id={'isActive-true'} label={'True'} value={true} 
			  onChange={e=> {
			 	setIsActive(true)
			 	console.log(isActive)
			 }}/>
			 <Form.Check name="radioActive" type={'radio'} id={'isActive-false'} value={false} label={'False'} onChange={e=> {
			 	setIsActive(false)
			 	console.log(isActive)
			 }}
			 defaultChecked/>
			 </>


			}
	

		 </Form.Group>
         
     
   
		</Form>
		

		<Button className="m-3" variant="warning" onClick={updateProduct} >Update</Button>
		



		</Card.Body>
		</Card>
		</Col>
		</Row>
		</Container>
		</>
		)
}
export {subTotal}