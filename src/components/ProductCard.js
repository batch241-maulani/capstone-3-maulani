import {useState, useEffect,useContext} from 'react'


import UserContext from '../UserContext'
import {Card,Row,Col,Button} from 'react-bootstrap'
import {Link, NavLink,Navigate} from 'react-router-dom'



export default function ProductCard({product}) {

	const {name, description, price,stock,imagelink, _id} = product
    const {user} = useContext(UserContext)

	const [isClicked,setIsClicked] = useState(false)

	function NavProduct() {
		if(user.isAdmin){
			{
				<Navigate to={`/products/${_id}/update`}/>
			}
		}else{
			{
				<Navigate to={`/products/${_id}/`}/>
			}
		}
	}

	const clickCard = ()=>{

    setIsClicked(true);
    }


	return (

				(isClicked) ? 	
					
					<Navigate to={user.isAdmin ? `/products/${_id}/update` : `/products/${_id}/`}/>

				:
				<>
				<Card onClick={clickCard} style={{cursor: 'pointer'}} className="cardHighlight p-3">
					<Card.Img variant="top" src={imagelink}/>
					<Card.Body>
					<Card.Title>
					<h2>{name}</h2>
					</Card.Title>
					<Card.Subtitle>
					<strong>Description:</strong>
					</Card.Subtitle>
					<Card.Text>
						{description}
					</Card.Text>
					<Card.Subtitle>
					Subtotal
					</Card.Subtitle>
					<Card.Text>
						PHP {price}
					</Card.Text>
			
					</Card.Body>
				</Card>
				</>
	
		
		)
}