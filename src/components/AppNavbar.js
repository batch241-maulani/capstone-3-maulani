import {useState, useEffect, useContext} from 'react'

import {Link, NavLink} from 'react-router-dom'

import UserContext from '../UserContext'

import Badge from 'react-bootstrap/Badge';

import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Image from "react-bootstrap/Image";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import NavDropdown from 'react-bootstrap/NavDropdown';

import {BiUserCircle} from 'react-icons/bi'
import {BsCart4} from 'react-icons/bs'

export default function AppNavbar() {
	
	const { user } = useContext(UserContext)

	const [hasUser,setHasUser] = useState(false)

	const [cartNumber,setCartNumber] = useState(0)

	let num = 0

	useEffect(()=>{
		if(user != null){
			setHasUser(true)
		}
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		}).then(res => res.json()).then(data=>{
			console.log(data)
			data.cart.items.forEach(item=>{
				num++
			})

			setCartNumber(num)

		})
	},[])
	
	const [show, setShow] = useState(false);
	const showDropdown = (e)=>{
		setShow(!show);
	}
	const hideDropdown = e => {
		setShow(false);
	}




	return(
		<Navbar className="black-bg-color" variant="dark" expand="lg">
		<Container >

		<Navbar.Brand href="#home" className="prompt-text">SMASMUNG</Navbar.Brand>
		<Navbar.Toggle aria-controls="basic-navbar-nav" />
		
		<Navbar.Collapse id="basic-navbar-nav" >
		<Nav className="me-auto">

		{
			(user.isAdmin) ?
			<> 
			{/*<Nav.Link href="#home">Home</Nav.Link>*/}
			<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
			<Nav.Link as={NavLink} to="/products/create">Create A Product</Nav.Link>
			<Nav.Link as={NavLink} to="/products/retrieveallproducts">All Products</Nav.Link>
				<Nav>
			<NavDropdown id="nav-dropdown-dark" show={show} onMouseEnter ={showDropdown} onMouseLeave={hideDropdown} title= {<BiUserCircle style={{height:30,width:30}}/>}menuVariant="Dark">
			{ 
				(user.id !== null) ?
				<NavDropdown.Item as={NavLink} to="/logout">Logout</NavDropdown.Item>

				:
				<>
				<NavDropdown.Item as={NavLink} to="/register"> Register </NavDropdown.Item>

				<NavDropdown.Item as={NavLink} to="/login">Login</NavDropdown.Item>
				</>


			}

			</NavDropdown>

			</Nav>
			</>
			:
			<>
			<Nav>
			{/*<Nav.Link href="#home">Home</Nav.Link>*/}
			<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
			</Nav>

			<Nav className="justify-content-end">
			<Nav.Link as={NavLink} to={'users/cart'} ><BsCart4 style={{height:30,width:30}}/><Badge pill bg="danger">
			{cartNumber}
			</Badge>{' '}</Nav.Link>
			<Nav>
			<NavDropdown id="nav-dropdown-dark" show={show} onMouseEnter ={showDropdown} onMouseLeave={hideDropdown} title= {<BiUserCircle style={{height:30,width:30}}/>}menuVariant="Dark">
			{ 
				(user.id !== null) ?
				<NavDropdown.Item as={NavLink} to="/logout">Logout</NavDropdown.Item>

				:
				<>
				<NavDropdown.Item as={NavLink} to="/register"> Register </NavDropdown.Item>

				<NavDropdown.Item as={NavLink} to="/login">Login</NavDropdown.Item>
				</>


			}

			</NavDropdown>

			</Nav>
			</Nav>
			</>


		}

		

		</Nav>


		

		</Navbar.Collapse>


		</Container>
		</Navbar>


		)


}

