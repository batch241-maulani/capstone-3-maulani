import {useState, useEffect, useContext} from 'react'

import { Form, Button } from 'react-bootstrap';

import {Navigate} from 'react-router-dom'

import UserContext from '../UserContext'

import Swal from 'sweetalert2'

export default function Login() {
    const {user,setUser} = useContext(UserContext)

    

    //State Hooks
    const [email,setEmail] = useState('')
    const [password,setPassword] = useState('')
    const [textWarning,setTextWarning] = useState(false)

    const [isActive,setIsActive] = useState(false)


    //const navigate = useNavigate()



    function loginUser(e) {

        e.preventDefault()

        console.log(process.env.REACT_APP_API_URL)

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // If no user information is found, the "access" property will not be available and return undefined
            if(typeof data.access !== "undefined"){
                // The JWT will be used to retrieve user information accross the whole frontend application and storing it in the localStorage
                localStorage.setItem('token', data.access)
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Smasmung!"
                })
            } else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Please, check your login details and try again."
                })
            }
        })

       // localStorage.setItem('email',email)
       // setUser({email: localStorage.getItem('email')})
       setEmail('')
       setPassword('')

   }


   const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);
            // Global user state for validation accross the whole app
            // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across whole application
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
          
        })



}




useEffect(() => {
    if(email !== "" && password !== "" ){
     setIsActive(true)
 } else{
     setIsActive(false)
 }

}, [email,password])

return (
    (user.id !== null) ?  
    <Navigate to= "/products"/>
    :
   
    <Form onSubmit={(e)=> loginUser(e)}>

    <Form.Group controlId="userEmail">
    <Form.Label>Email address</Form.Label>

    <Form.Control 
    type="email" 
    placeholder="Enter email" 
    value= {email}
    onChange = {e => setEmail(e.target.value)}
    required
    />
    {
        textWarning ?
        <Form.Text className="text-danger">
        Invalid email or password
        </Form.Text>
        :
        <Form.Text className="text-danger">

        </Form.Text>


    }


    </Form.Group>

    <Form.Group controlId="password">
    <Form.Label>Password</Form.Label>
    <Form.Control 
    type="password" 
    placeholder="Password" 
    value= {password}
    onChange = {e => setPassword(e.target.value)}
    required
    />
    </Form.Group>
    {
        isActive ?
        <Button variant="success" type="submit" id="submitBtn">
        Login
        </Button>
        :
        <Button variant="success" type="submit" id="submitBtn" disabled>
        Login
        </Button>
    }
    </Form>
    )

}