import {useState,useEffect} from 'react'

import ProductCard from '../components/ProductCard'
import {Card,Row,Col} from 'react-bootstrap'
import {Button} from 'react-bootstrap'


export default function Products() {

	const [products,setProducts] = useState([])

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/products/activeproducts`).then(res => res.json()).then(data=> {
			console.log(data)
			setProducts(data.map(product=>{
				return(
				
				
					
					<Col xs={12} md={3}>
					<ProductCard key={product._id} product =  {product}/>
					</Col>
				

				)
			}))
		})
	},[])



	return (
		<>	
			
			<Row className="mb-3 mt-3">
				
				{products}
				
			</Row>
		</>
	)



}
