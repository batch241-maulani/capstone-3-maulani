import {useState,useEffect} from 'react'

import ProductCard from '../components/ProductCard'
import {Card,Row,Col} from 'react-bootstrap'
import {Button} from 'react-bootstrap'


export default function AllProducts() {

	const [products,setProducts] = useState([])
    const token = localStorage.getItem('token')
	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/products/retrieveallproducts`,{
    		'method': 'POST',
    		headers:{
    			'Authorization': `Bearer ${token}`
    		}}).then(res => res.json()).then(data=> {
			console.log(data)
			setProducts(data.map(product=>{
				return(
					
					<Col xs={12} md={3}>
					<ProductCard key={product._id} product =  {product}/>
					</Col>
				

				)
			}))
		})
	},[])



	return (
		<>	
			
			<Row className="mb-3 mt-3">
				
				{products}
				
			</Row>
		</>
	)



}