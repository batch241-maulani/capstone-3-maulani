import {useState, useEffect,useContext} from 'react'

import { Form, Button } from 'react-bootstrap';

import UserContext from '../UserContext'

import {Navigate} from 'react-router-dom'

import Swal from 'sweetalert2'

export default function Register() {
	const {user,setUser} = useContext(UserContext)



    //State Hooks
    const [firstName,setFirstName] = useState('')
    const [lastName,setLastName] = useState('')
    const [email,setEmail] = useState('')
    const [password1,setPassword1] = useState('')
    const [password2,setPassword2] = useState('')

    const [isActive,setIsActive] = useState(false)

    const [isExist,setIsExist] = useState(false)
    
    const userAlreadyExist = (email) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`,{
            'method': 'POST',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        }).then(res => res.json()).then(data=> {
            console.log('This is the result of data:' +data)
            if(data) {
                setIsExist(true)
                console.log('This is the result of data isExist:' + isExist)
            }else{
                setIsExist(false)
            }
        })
    }

    function registerUser(e) {
        e.preventDefault()

     if(isExist){
         Swal.fire({
            title: "Authentication Failed",
            icon: "error",
            text: "User already Registered!"
        })

     }else{

         fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
            'method': 'POST',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password1
            })
        }).then(res => res.json()).then(data=> {
            console.log(data)
           
                Swal.fire({
                    title: "Registered Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })
             
        })
    }

    setFirstName('')
    setLastName('')
    setEmail('')
    setPassword1('')
    setPassword2('')

}

useEffect(() => {

    if(email !== "" && password1 !== "" && password2 !== "" && firstName !== "" && lastName !== "" && password1 === password2){
       setIsActive(true)
       userAlreadyExist(email)
   } else{
       setIsActive(false)
   }

}, [firstName, lastName,email,password1,password2])

return (
	(user.id !== null) ?
    <Navigate to= "/products"/>
  	:
    <Form onSubmit={(e)=> registerUser(e)}>
    <Form.Group controlId="userFirstName">
    <Form.Label>First Name</Form.Label>
    <Form.Control 
    type="text" 
    placeholder="Enter First Name" 
    value= {firstName}
    onChange = {e => setFirstName(e.target.value)}
    required
    />
    </Form.Group>
    <Form.Group controlId="userLastName">
    <Form.Label>Last Name</Form.Label>
    <Form.Control 
    type="text" 
    placeholder="Enter Last Name" 
    value= {lastName}
    onChange = {e => setLastName(e.target.value)}
    required
    />
    </Form.Group>

    <Form.Group controlId="userEmail">
    <Form.Label>Email address</Form.Label>
    <Form.Control 
    type="email" 
    placeholder="Enter email" 
    value= {email}
    onChange = {e => setEmail(e.target.value)}
    required
    />
    <Form.Text className="text-muted">
    We'll never share your email with anyone else.
    </Form.Text>
    </Form.Group>

    <Form.Group controlId="password1">
    <Form.Label>Password</Form.Label>
    <Form.Control 
    type="password" 
    placeholder="Password" 
    value= {password1}
    onChange = {e => setPassword1(e.target.value)}
    required
    />
    </Form.Group>

    <Form.Group controlId="password2">
    <Form.Label>Verify Password</Form.Label>
    <Form.Control 
    type="password" 
    placeholder="Verify Password" 
    value= {password2}
    onChange = {e => setPassword2(e.target.value)}
    required
    />
    </Form.Group>

    {
        isActive ?

        <Button variant="primary" type="submit" id="submitBtn">
        Submit
        </Button>
        :

        <Button variant="danger" type="submit" id="submitBtn">
        Submit
        </Button>


    }


    </Form>
    )

}