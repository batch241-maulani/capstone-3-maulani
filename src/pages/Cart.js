import {useState,useEffect,useContext} from 'react'

import ProductCard from '../components/ProductCard'
import {Card,Row,Col} from 'react-bootstrap'
import UserContext from '../UserContext'
import CartCard from '../components/CartCard'
import { Form } from 'react-bootstrap';

import {Button} from 'react-bootstrap'

import Swal from 'sweetalert2'

export default function Cart() {

	const { user } = useContext(UserContext)
	const [products,setProducts] = useState([])
	const [totalAmount,setTotalAmount] = useState(0)

	const token = localStorage.getItem('token')
	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}`).then(res => res.json()).then(data=> {
			console.log('I fire once')
			setTotalAmount(data.cart.totalAmount)
			console.log(data)
			
			setProducts(data.cart.items.map(product=>{
				return(
				
					<CartCard key={product._id} product =  {product}/>
					
				)
			}))
		})
	},[])


	function checkOutCart(e) {
		e.preventDefault()

		

    	fetch(`${process.env.REACT_APP_API_URL}/users/checkout`,{
    		'method': 'POST',
    		headers:{
    			Authorization: `Bearer ${token}`,
    			'Content-Type': 'application/json'
    		},
    		
    	}).then(res => res.json()).then(data=> {
    		console.log(data)

    		Swal.fire({
    			title: "Checkout Successfully!",
    			icon: "success",
    			text: "Ordered."
    		})

    	})
	}

	

	return (
		<>	
		<Row className="mt-3">
		<Col>
		<Form.Label>Total Amount: {totalAmount} </Form.Label>
		<Button className="m-3" onClick={checkOutCart}  variant="danger" ><strong>Check Out </strong></Button>
		</Col>
		</Row>
		<Row className="mb-3 mt-3">
				<Col xs={12} md={12}>
				{products}
				</Col>
		</Row>
		</>
	)



}