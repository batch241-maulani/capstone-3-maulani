import {useState, useEffect,useContext} from 'react'

import { Form, Button } from 'react-bootstrap';

import UserContext from '../UserContext'

import {Navigate} from 'react-router-dom'

import Swal from 'sweetalert2'

export default function CreateProduct() {
	const {user,setUser} = useContext(UserContext)



    //State Hooks
    const [name,setName] = useState('')
    const [description,setDescription] = useState('')
    const [stock,setStock] = useState(0)
    const [price,setPrice] = useState(0)
    const [imagelink,setImageLink] = useState('')

    const [isActive,setIsActive] = useState(false)


    const token = localStorage.getItem('token')
    function createProduct(e) {
    	e.preventDefault()



    	fetch(`${process.env.REACT_APP_API_URL}/products/create`,{
    		'method': 'POST',
    		headers:{
    			Authorization: `Bearer ${token}`,
    			'Content-Type': 'application/json'
    		},
    		body: JSON.stringify({
    			name: name,
    			description: description,
    			stock: stock,
    			price: price,
    			imagelink: imagelink
    		})
    	}).then(res => res.json()).then(data=> {
    		console.log(data)

    		Swal.fire({
    			title: "Product Added Successfully!",
    			icon: "success",
    			text: "Added."
    		})

    	})


    	setName('')
    	setDescription('')
    	setStock('')
    	setPrice('')
    	setImageLink('')


    }

    useEffect(() => {

    	if(name !== "" && description !== "" && stock !== "" && price !== ""){
    		setIsActive(true)
    	} else{
    		setIsActive(false)
    	}

    }, [name, description,stock,price,imagelink])

    return (

    	<Form onSubmit={(e)=> createProduct(e)}>

    	<Form.Group controlId="productName">
    	<Form.Label>Name</Form.Label>
    	<Form.Control 
    	type="text" 
    	placeholder="Enter Product Name" 
    	value= {name}
    	onChange = {e => setName(e.target.value)}
    	required
    	/>
    	</Form.Group>

    	<Form.Group controlId="productDescription">
    	<Form.Label>Description</Form.Label>
    	<Form.Control 
    	type="text" 
    	placeholder="Enter Description" 
    	value= {description}
    	onChange = {e => setDescription(e.target.value)}
    	required
    	/>
    	</Form.Group>

    	<Form.Group controlId="productStock">
    	<Form.Label>Stock</Form.Label>
    	<Form.Control 
    	type="number" 
    	placeholder="Enter Stock" 
    	value= {stock}
    	onChange = {e => setStock(e.target.value)}
    	required
    	/>
    	</Form.Group>


    	<Form.Group controlId="productPrice">
    	<Form.Label>Price</Form.Label>
    	<Form.Control 
    	type="number" 
    	placeholder="Price" 
    	value= {price}
    	onChange = {e => setPrice(e.target.value)}
    	required
    	/>
    	</Form.Group>

    	<Form.Group controlId="productImageLink">
    	<Form.Label>Image (Link)</Form.Label>
    	<Form.Control 
    	type="text" 
    	placeholder="Enter Description" 
    	value= {imagelink}
    	onChange = {e => setImageLink(e.target.value)}
    	required
    	/>
    	</Form.Group>

    	{
    		isActive ?

    		<Button variant="primary" type="submit" id="submitBtn">
    		Submit
    		</Button>

    		:

    		<Button variant="danger" type="submit" id="submitBtn">
    		Submit
    		</Button>
    	}

    	</Form>


    	)

}