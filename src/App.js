import './App.css';


import {useState,useEffect} from 'react'


import AppNavbar from './components/AppNavbar'
import ProductView from './components/ProductView'
import SingleCheckOutView from './components/SingleCheckOutView'
import CartCard from './components/CartCard'
import UpdateProduct from './components/UpdateProduct'


import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Products from './pages/Products'
import Cart from './pages/Cart'
import CreateProduct from './pages/CreateProduct'
import AllProducts from './pages/AllProducts'

import {Container} from 'react-bootstrap'

import {UserProvider} from './UserContext'

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom' 



function App() {
  const [user,setUser] = useState({
    id: null,
    isAdmin: null
  })



  const unsetUser = () => {
      localStorage.clear()
  }

  useEffect(()=>{
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
      headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    }).then(res => res.json()).then(data=>{
      console.log(data)
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else {
        setUser({
          id: null,
          isAdmin: null
        })
      }

    })
  },[])
    
  return (
   <>
    <UserProvider value = {{user,setUser,unsetUser}}>
     <Router>
       <AppNavbar/>
       <Container>
          <Routes>
           <Route path="/register" element={ <Register/>}/>
           <Route path="/login" element={ <Login/>}/>
           <Route path="/logout" element={ <Logout/>}/>
           <Route path="/products" element={ <Products/>}/>
           <Route path="/products/:_id" element={ <ProductView/>}/>
           <Route path="/products/:_id/update" element={ <UpdateProduct/>}/>
            <Route path="/users/order" element={ <SingleCheckOutView/>}/>
            <Route path="/users/cart" element={ <Cart/>}/>
           <Route path="/products/create" element={ <CreateProduct/>}/>
             <Route path="/products/retrieveallproducts" element={ <AllProducts/>}/>
          
         
    
         </Routes>
       </Container>
     </Router>
    </UserProvider>
    </>




    )
}

export default App;
